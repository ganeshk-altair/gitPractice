# Acoustic Tet Mesh - Adaptive Perfectly Matched Layer 

# ##### SET Paths - START ###########
export ALTAIR_LICENSE_PATH=
# : Delivery Path eg - D:\SimLab_AcousticTetMesh\bin\win64
export SIMLAB_BIN=
# To avoid clearing Temp : SET SF_LICENSE=123
#export SF_LICENSE=123
# ##### SET Paths - DONE ############

echo Acoustic Tet Mesh RTest Started...

$SIMLAB_BIN/SimLab_APML.sh "input/sphere" "output/sphere_out" "5.0" "2.0" "1.1"
$SIMLAB_BIN/SimLab_APML.sh "input/cube" "output/cube_1_out" "5.0" "2.0" "1.5"
$SIMLAB_BIN/SimLab_APML.sh "input/cube" "output/cube_2_out" "5.0" "2.0" "1.1"
$SIMLAB_BIN/SimLab_APML.sh "input/cube" "output/cube_3_out" "2.0" "0.5" "1.2"

# *** Infinite Loop bug
$SIMLAB_BIN/SimLab_APML.sh "input/cube_infiniteLoop_issue" "output/cube_infiniteLoop_out" "0.37" "0.1" "1.2"

# *** Intersections bug
$SIMLAB_BIN/SimLab_APML.sh "input/renault_final.pml_inp_0" "output/renault_final.pml_out" "850.0" "212.5" "2.0"

echo Acoustic Tet Mesh RTest Completed...
#pause

# 1. Node ids of input faces will be retained
# 2. Sets Format using thru
#    Eg SET*    1               GRID            LIST
#       *       2249            THRU            3284 

