REM Acoustic Tet Mesh - Adaptive Perfectly Matched Layer 

REM ##### SET Paths - START ###########
SET ALTAIR_LICENSE_PATH=
REM : Delivery Path eg - D:\SimLab_AcousticTetMesh\bin\win64
SET SIMLAB_BIN=
REM To avoid clearing Temp : SET SF_LICENSE=123
REM ##### SET Paths - DONE ############

echo Acoustic Tet Mesh RTest Started...

call %SIMLAB_BIN%/SimLab_APML.bat "input/sphere" "output/sphere_out" "5.0" "2.0" "1.1"
call %SIMLAB_BIN%/SimLab_APML.bat "input/cube" "output/cube_1_out" "5.0" "2.0" "1.5"
call %SIMLAB_BIN%/SimLab_APML.bat "input/cube" "output/cube_2_out" "5.0" "2.0" "1.1"
call %SIMLAB_BIN%/SimLab_APML.bat "input/cube" "output/cube_3_out" "2.0" "0.5" "1.2"

REM *** Infinite Loop bug
call %SIMLAB_BIN%/SimLab_APML.bat "input/cube_infiniteLoop_issue" "output/cube_infiniteLoop_out" "0.37" "0.1" "1.2"

REM *** Intersections bug
call %SIMLAB_BIN%/SimLab_APML.bat "input/renault_final.pml_inp_0" "output/renault_final.pml_out" "850.0" "212.5" "2.0"

echo Acoustic Tet Mesh RTest Completed...
pause

REM 1. Node ids of input faces will be retained
REM 2. Sets Format using thru
REM    Eg SET*    1               GRID            LIST
REM       *       2249            THRU            3284 

